// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');
var bodyParser = require('body-parser');
var app        = express();
var pg = require('pg');
const pool = require('./Util/db');
//const connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/todo';
// configure body parser
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port     = process.env.PORT || 8080; // set our port

// ROUTES FOR OUR API
// =============================================================================

// create our router
var router = express.Router();

// middleware to use for all requests
router.use(function(req, res, next) {
	// do logging
	console.log('Something is happening.');
	next();
});




router.get('/api/v1/areas', (req, res, next) => {
  const results = [];
  // Get a Postgres client from the connection pool
  pg.connect(function (err, client, done) {
    console.log('********API')
    // Handle connection errors
    if(err) {
      done();
	    console.log('error')
      console.log(err);
      return res.status(500).json({success: false, data: err});
    }
    // // SQL Query > Select Data
    // const query = client.query('SELECT * FROM area ORDER BY id ASC;');
    // // Stream results back one row at a time
    // query.on('row', (row) => {
    //   results.push(row);
    // });
    // // After all data is returned, close connection and return results
    // query.on('end', () => {
    //   done();
    //   return res.json(results);
    // });
  });
});


// test route to make sure everything is working (accessed at GET http://localhost:8080)

router.get('/api', function(req, res) {
	res.json({ message: 'Cable Vision api!' });	
});


// REGISTER OUR ROUTES -------------------------------
app.use('/', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);
